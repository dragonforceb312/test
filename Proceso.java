/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.carrito;
import java.util.ArrayList;
/**
 *
 * @author drago
 */
public class Proceso {
    private ArrayList<Usuario>nombre_list;
    private ArrayList<Canasta>usuarios;
    private Articulos Carnes, Frutas, Pastas, Aves, Lacteos, Licores, Limpieza, Panaderia;
    private Usuario usuario;
    public Proceso(){
        cargarUsuarios();
        cargarArticulos();
        usuarios = new ArrayList<Canasta>();
    }
    
    public Usuario validarClave(String user, int clave){
        Usuario validar =  null;
        if(usuarios.size()>0){
            for (Canasta canasta : usuarios) {
                if(canasta.getUsuario().getNombre().equals(user) && canasta.getUsuario().getClave()==clave){
                    return canasta.getUsuario();
                }
            }
        }
        for(int i=0; i<nombre_list.size(); i++){
            System.out.println(nombre_list.get(i).getNombre().equals(user) && nombre_list.get(i).getClave()==clave);
            if(nombre_list.get(i).getNombre().equals(user) && nombre_list.get(i).getClave()==clave){
                return nombre_list.get(i);
            }
        }

        return validar;
    }
    
    public Canasta getCanasta(Usuario usuario){
         Canasta validar =  null;
        if(usuarios.size()>0){
            for (Canasta canasta : usuarios) {
                if(canasta.getUsuario().getNombre().equals(usuario.getNombre()) && canasta.getUsuario().getClave()==usuario.getClave()){
                    validar = canasta;
                    break;
                }
            }
        }
        return validar;
    }
    
    public boolean validarUsuario(String user){
        for(int i=0; i<nombre_list.size(); i++){
            if(nombre_list.get(i).getNombre().equals(user)){
                return true;
            }
        }
        return false;
    }
    
    
    public void cargarUsuarios(){
        nombre_list = new ArrayList<Usuario>();  
        nombre_list.add(new Usuario ("Hugo", 123, 1000, 1000, "Frecuente", 10));
        nombre_list.add(new Usuario ("Paco",312, 1500, 1500, " Plus", 15));
        nombre_list.add(new Usuario ("Luis", 343, 500, 500, " ocasional", 5));
    }
    
    public void cargarArticulos(){
        setCarnes(new Articulos (5151,"Area de Carne",50, 90));
        setFrutas(new Articulos (5252, "Area de Frutas",20, 50));
        setPastas(new Articulos (5353,"Area de Pastas",30, 30));
        setAves(new Articulos (5454, "Area de aves, pollo, etc.", 40, 60));
        setLacteos(new Articulos (5555,"Area de Lacteos y deslactozados",50 ,100));
        setLicores(new Articulos (5656, "Area de Licores (+18)", 100, 100));
        setLimpieza(new Articulos (5757, "Area de Limpieza",200, 80));
        setPanaderia(new Articulos (5858, "Panaderia: Area de ´panes y postres",200, 60));
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Articulos getCarnes() {
        return Carnes;
    }

    public void setCarnes(Articulos Carnes) {
        this.Carnes = Carnes;
    }

    public Articulos getFrutas() {
        return Frutas;
    }

    public void setFrutas(Articulos Frutas) {
        this.Frutas = Frutas;
    }

    public Articulos getPastas() {
        return Pastas;
    }

    public void setPastas(Articulos Pastas) {
        this.Pastas = Pastas;
    }

    public Articulos getAves() {
        return Aves;
    }

    public void setAves(Articulos Aves) {
        this.Aves = Aves;
    }

    public Articulos getLacteos() {
        return Lacteos;
    }

    public void setLacteos(Articulos Lacteos) {
        this.Lacteos = Lacteos;
    }

    public Articulos getLicores() {
        return Licores;
    }

    public void setLicores(Articulos Licores) {
        this.Licores = Licores;
    }

    public Articulos getLimpieza() {
        return Limpieza;
    }

    public void setLimpieza(Articulos Limpieza) {
        this.Limpieza = Limpieza;
    }

    public Articulos getPanaderia() {
        return Panaderia;
    }

    public void setPanaderia(Articulos Panaderia) {
        this.Panaderia = Panaderia;
    }

    public ArrayList<Canasta> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(ArrayList<Canasta> usuarios) {
        this.usuarios = usuarios;
    }
}
