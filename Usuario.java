/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.carrito;

/**
 *
 * @author drago
 */
public class Usuario {

    private String Nombre;
    private int clave;
    private float Saldo_inicial_en_pesos;
    private float Saldo_final_en_pesos;
    private String Eres_un_cliente;
    private int porcentaje_de_descuento;
    
    public Usuario(){
    }

    public Usuario(String Nombre, int clave, float Saldo_inicial_en_pesos, float Saldo_final_en_pesos, String Eres_un_cliente, int porcentaje_de_descuento){
        
       
        this.Nombre = Nombre;
        this.clave = clave;
        this.Saldo_inicial_en_pesos = Saldo_inicial_en_pesos;
        this.Eres_un_cliente = Eres_un_cliente;
        this.porcentaje_de_descuento = porcentaje_de_descuento;
        this.Saldo_final_en_pesos = Saldo_final_en_pesos;

    
    }

    
    @Override
    public String toString(){
        return "Usuario: " + getNombre() + "\tClave: " + getClave() +
        "\tSaldo en pesos: " + getSaldo_inicial_en_pesos() +"\tEres un cliente: " + getEres_un_cliente() +"\tPorcentaje de descuento: " + getPorcentaje_de_descuento()+"%";
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public float getSaldo_inicial_en_pesos() {
        return Saldo_inicial_en_pesos;
    }

    public void setSaldo_inicial_en_pesos(float Saldo_inicial_en_pesos) {
        this.Saldo_inicial_en_pesos = Saldo_inicial_en_pesos;
    }

    public float getSaldo_final_en_pesos() {
        return Saldo_final_en_pesos;
    }

    public void setSaldo_final_en_pesos(float Saldo_final_en_pesos) {
        this.Saldo_final_en_pesos = Saldo_final_en_pesos;
    }

    public String getEres_un_cliente() {
        return Eres_un_cliente;
    }

    public void setEres_un_cliente(String Eres_un_cliente) {
        this.Eres_un_cliente = Eres_un_cliente;
    }

    public int getPorcentaje_de_descuento() {
        return porcentaje_de_descuento;
    }

    public void setPorcentaje_de_descuento(int porcentaje_de_descuento) {
        this.porcentaje_de_descuento = porcentaje_de_descuento;
    }
}
