/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.carrito;

import java.util.ArrayList;

/**
 *
 * @author drago
 */
public class Canasta {
    private Usuario usuario;
    private float precio_total, precio_canasta;
    private int aves, panaderia, limpieza, pastas, frutas, licores, carnes, lacteos;
    private ArrayList<Articulos> avesCarrito, panaderiaCarrito, licoresCarrito, limpiezaCarrito, carnesCarrito, pastasCarrito, frutasCarrito, lacteosCarrito;
    private ArrayList<Articulos> avesComprados, panaderiaComprados, licoresComprados, limpiezaComprados, carnesComprados, pastasComprados, frutasComprados, lacteosComprados;
    
    public Canasta(){
        avesCarrito = new ArrayList<Articulos>(); 
        panaderiaCarrito = new ArrayList<Articulos>(); 
        licoresCarrito = new ArrayList<Articulos>();
        limpiezaCarrito = new ArrayList<Articulos>(); 
        carnesCarrito = new ArrayList<Articulos>();
        pastasCarrito = new ArrayList<Articulos>();
        frutasCarrito = new ArrayList<Articulos>();
        lacteosCarrito = new ArrayList<Articulos>();
        avesComprados = new ArrayList<Articulos>();
        panaderiaComprados = new ArrayList<Articulos>();
        licoresComprados = new ArrayList<Articulos>();
        limpiezaComprados = new ArrayList<Articulos>();
        carnesComprados = new ArrayList<Articulos>();
        pastasComprados = new ArrayList<Articulos>();
        frutasComprados = new ArrayList<Articulos>();
        lacteosComprados = new ArrayList<Articulos>();
        precio_total = 0;
        limpiar();
    }
    
    public void limpiar(){
        precio_canasta = 0;
        aves = 0; 
        panaderia = 0;
        limpieza = 0; 
        pastas = 0;
        frutas = 0; 
        licores = 0; 
        carnes = 0;
        lacteos = 0;
    }
    
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public float getPrecio_total() {
        return precio_total;
    }

    public void setPrecio_total(float precio_total) {
        this.precio_total = precio_total;
    }

    public ArrayList<Articulos> getAvesComprados() {
        return avesComprados;
    }

    public void setAvesComprados(ArrayList<Articulos> avesComprados) {
        this.avesComprados = avesComprados;
    }

    public ArrayList<Articulos> getPanaderiaComprados() {
        return panaderiaComprados;
    }

    public void setPanaderiaComprados(ArrayList<Articulos> panaderiaComprados) {
        this.panaderiaComprados = panaderiaComprados;
    }

    public ArrayList<Articulos> getLicoresComprados() {
        return licoresComprados;
    }

    public void setLicoresComprados(ArrayList<Articulos> licoresComprados) {
        this.licoresComprados = licoresComprados;
    }

    public ArrayList<Articulos> getLimpiezaComprados() {
        return limpiezaComprados;
    }

    public void setLimpiezaComprados(ArrayList<Articulos> limpiezaComprados) {
        this.limpiezaComprados = limpiezaComprados;
    }

    public ArrayList<Articulos> getCarnesComprados() {
        return carnesComprados;
    }

    public void setCarnesComprados(ArrayList<Articulos> carnesComprados) {
        this.carnesComprados = carnesComprados;
    }

    public ArrayList<Articulos> getPastasComprados() {
        return pastasComprados;
    }

    public void setPastasComprados(ArrayList<Articulos> pastasComprados) {
        this.pastasComprados = pastasComprados;
    }

    public ArrayList<Articulos> getFrutasComprados() {
        return frutasComprados;
    }

    public void setFrutasComprados(ArrayList<Articulos> frutasComprados) {
        this.frutasComprados = frutasComprados;
    }

    public ArrayList<Articulos> getLacteosComprados() {
        return lacteosComprados;
    }

    public void setLacteosComprados(ArrayList<Articulos> lacteosComprados) {
        this.lacteosComprados = lacteosComprados;
    }

    public ArrayList<Articulos> getAvesCarrito() {
        return avesCarrito;
    }

    public void setAvesCarrito(ArrayList<Articulos> avesCarrito) {
        this.avesCarrito = avesCarrito;
    }

    public ArrayList<Articulos> getPanaderiaCarrito() {
        return panaderiaCarrito;
    }

    public void setPanaderiaCarrito(ArrayList<Articulos> panaderiaCarrito) {
        this.panaderiaCarrito = panaderiaCarrito;
    }

    public ArrayList<Articulos> getLicoresCarrito() {
        return licoresCarrito;
    }

    public void setLicoresCarrito(ArrayList<Articulos> licoresCarrito) {
        this.licoresCarrito = licoresCarrito;
    }

    public ArrayList<Articulos> getLimpiezaCarrito() {
        return limpiezaCarrito;
    }

    public void setLimpiezaCarrito(ArrayList<Articulos> limpiezaCarrito) {
        this.limpiezaCarrito = limpiezaCarrito;
    }

    public ArrayList<Articulos> getCarnesCarrito() {
        return carnesCarrito;
    }

    public void setCarnesCarrito(ArrayList<Articulos> carnesCarrito) {
        this.carnesCarrito = carnesCarrito;
    }

    public ArrayList<Articulos> getPastasCarrito() {
        return pastasCarrito;
    }

    public void setPastasCarrito(ArrayList<Articulos> pastasCarrito) {
        this.pastasCarrito = pastasCarrito;
    }

    public ArrayList<Articulos> getFrutasCarrito() {
        return frutasCarrito;
    }

    public void setFrutasCarrito(ArrayList<Articulos> frutasCarrito) {
        this.frutasCarrito = frutasCarrito;
    }

    public ArrayList<Articulos> getLacteosCarrito() {
        return lacteosCarrito;
    }

    public void setLacteosCarrito(ArrayList<Articulos> lacteosCarrito) {
        this.lacteosCarrito = lacteosCarrito;
    }

    public int getAves() {
        return aves;
    }

    public void setAves(int aves) {
        this.aves = aves;
    }

    public int getPanaderia() {
        return panaderia;
    }

    public void setPanaderia(int panaderia) {
        this.panaderia = panaderia;
    }

    public int getLimpieza() {
        return limpieza;
    }

    public void setLimpieza(int limpieza) {
        this.limpieza = limpieza;
    }

    public int getPastas() {
        return pastas;
    }

    public void setPastas(int pastas) {
        this.pastas = pastas;
    }

    public int getFrutas() {
        return frutas;
    }

    public void setFrutas(int frutas) {
        this.frutas = frutas;
    }

    public int getLicores() {
        return licores;
    }

    public void setLicores(int licores) {
        this.licores = licores;
    }

    public int getCarnes() {
        return carnes;
    }

    public void setCarnes(int carnes) {
        this.carnes = carnes;
    }

    public int getLacteos() {
        return lacteos;
    }

    public void setLacteos(int lacteos) {
        this.lacteos = lacteos;
    }

    public float getPrecio_canasta() {
        return precio_canasta;
    }

    public void setPrecio_canasta(float precio_canasta) {
        this.precio_canasta = precio_canasta;
    }
}