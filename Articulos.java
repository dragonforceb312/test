/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.carrito;

/**
 *
 * @author drago
 */
public class Articulos {

    private int id;
    private String Producto;
    private int kilos;
    private double PrecioKG;

    public Articulos() {
    }
    
    public Articulos (int id, String Producto, int kilos, double PrecioKG){
        this.Producto = Producto;
        this.PrecioKG = PrecioKG;
        this.kilos = kilos;
        this.id = id;
    }
    public int getid(){
        return id;
    }
    
    public void setid(int id){
        this.id = id;
    }
    
    public String getProducto(){
        return Producto;
        
    }
    
    public void setProducto(String Producto){
        this.Producto = Producto;
    }
    
    public int getkilos (){
            return kilos;
        }
    public void setkilos(int kilos){
        this.kilos = kilos;
    }
    public double getPrecioKG(){
        return PrecioKG;
    }
    
    public void setPrecioKG (double PrecioKG){
        this.PrecioKG = PrecioKG;
    
    }
    
       public void aumentaExistencia(int valor) {
        this.kilos += valor;
    }
    
    public void disminuyeExistenca(int valor) {
        this.kilos -=  valor;
    }
  
    @Override
    public String toString() {
        return "Articulos{" + "id=" + id + ", Producto=" + Producto + ", kilos=" + kilos + ", PrecioKG" + PrecioKG + '}';
    }
}
