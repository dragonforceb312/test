/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.carrito;

import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

/**
 *
 * @author drago
 */
public class Carrito extends javax.swing.JFrame {
    private Proceso p;
    private Canasta canasta;
    private float descuento;
    private ArrayList<Articulos> lista_articulos;
    private ArrayList<Articulos> articulos_comprados;
    /**
     * Creates new form Home
     */
    public Carrito(Proceso p, Canasta canasta) {
        initComponents();
        this.p = p;
        descuento = (float) (p.getUsuario().getPorcentaje_de_descuento()*1.0/100);
        this.canasta = canasta;
        lista_articulos = new ArrayList<Articulos>();
        articulos_comprados = new ArrayList<Articulos>();
        nombre.setText(p.getUsuario().getNombre());
        carnesPrecio.setText("$"+precio_descuento(canasta.getCarnes(), (float) p.getCarnes().getPrecioKG()));
        frutasPrecio.setText("$"+precio_descuento(canasta.getFrutas(), (float) p.getCarnes().getPrecioKG()));
        pastasPrecio.setText("$"+precio_descuento(canasta.getPastas(), (float) p.getCarnes().getPrecioKG()));
        licoresPrecio.setText("$"+precio_descuento(canasta.getLicores(), (float) p.getCarnes().getPrecioKG()));
        limpiezaPrecio.setText("$"+precio_descuento(canasta.getLimpieza(), (float) p.getCarnes().getPrecioKG()));
        panaderiaPrecio.setText("$"+precio_descuento(canasta.getPanaderia(), (float) p.getCarnes().getPrecioKG()));
        avesPrecio.setText("$"+precio_descuento(canasta.getAves(), (float) p.getCarnes().getPrecioKG()));
        lacteosPrecio.setText("$"+precio_descuento(canasta.getLacteos(), (float) p.getCarnes().getPrecioKG()));
        total.setText("$"+canasta.getPrecio_canasta());
        cargarSpinner();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel11 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        nombre = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        frutasPrecio = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        pastasPrecio = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        avesPrecio = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lacteosPrecio = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        licoresPrecio = new javax.swing.JLabel();
        limpiezaPrecio = new javax.swing.JLabel();
        carneCont = new javax.swing.JSpinner();
        frutasCont = new javax.swing.JSpinner();
        pastasCont = new javax.swing.JSpinner();
        avesCont = new javax.swing.JSpinner();
        lacteosCont = new javax.swing.JSpinner();
        limpiezaCont = new javax.swing.JSpinner();
        licoresCont = new javax.swing.JSpinner();
        panaderiaCont = new javax.swing.JSpinner();
        jLabel2 = new javax.swing.JLabel();
        panaderiaPrecio = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        carnesPrecio = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        total = new javax.swing.JLabel();
        comprar = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        jMenu5 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();

        jLabel11.setText("Panadería");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Nombre : ");

        nombre.setText("jLabel2");

        jLabel4.setText("Pastas");

        frutasPrecio.setText("$50");

        jLabel5.setText("Aves");

        pastasPrecio.setText("$25");

        jLabel6.setText("Lícores");

        avesPrecio.setText("$60");

        jLabel7.setText("Limpiezas");

        lacteosPrecio.setText("$100");

        jLabel8.setText("Panadería");

        licoresPrecio.setText("$100");

        limpiezaPrecio.setText("$80");

        carneCont.setModel(new javax.swing.SpinnerNumberModel(1, 1, 50, 1));
        carneCont.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                carneContStateChanged(evt);
            }
        });

        frutasCont.setModel(new javax.swing.SpinnerNumberModel(1, 1, 20, 1));
        frutasCont.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                frutasContStateChanged(evt);
            }
        });

        pastasCont.setModel(new javax.swing.SpinnerNumberModel(1, 1, 30, 1));
        pastasCont.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                pastasContStateChanged(evt);
            }
        });

        avesCont.setModel(new javax.swing.SpinnerNumberModel(1, 1, 40, 1));
        avesCont.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                avesContStateChanged(evt);
            }
        });

        lacteosCont.setModel(new javax.swing.SpinnerNumberModel(1, 1, 50, 1));
        lacteosCont.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                lacteosContStateChanged(evt);
            }
        });

        limpiezaCont.setModel(new javax.swing.SpinnerNumberModel(1, 1, 200, 1));
        limpiezaCont.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                limpiezaContStateChanged(evt);
            }
        });

        licoresCont.setModel(new javax.swing.SpinnerNumberModel(1, 1, 100, 1));
        licoresCont.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                licoresContStateChanged(evt);
            }
        });

        panaderiaCont.setModel(new javax.swing.SpinnerNumberModel(1, 1, 200, 1));
        panaderiaCont.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                panaderiaContStateChanged(evt);
            }
        });

        jLabel2.setText("Carnes");

        panaderiaPrecio.setText("$60");

        jLabel3.setText("Frutas");

        carnesPrecio.setText("$90");

        jLabel18.setText("Lácteos");

        jLabel19.setText("Productos de compra pendiente");

        jLabel9.setText("Total :  ");

        total.setText("jLabel10");

        comprar.setText("Comprar");
        comprar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprarActionPerformed(evt);
            }
        });

        jMenu3.setText("Saldo");
        jMenu3.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
            public void menuDeselected(javax.swing.event.MenuEvent evt) {
            }
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                jMenu3MenuSelected(evt);
            }
        });
        jMenuBar1.add(jMenu3);

        jMenu5.setText("Lista de productos");
        jMenu5.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
            public void menuDeselected(javax.swing.event.MenuEvent evt) {
            }
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                jMenu5MenuSelected(evt);
            }
        });
        jMenuBar1.add(jMenu5);

        jMenu2.setText("Compras");
        jMenu2.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
            public void menuDeselected(javax.swing.event.MenuEvent evt) {
            }
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                jMenu2MenuSelected(evt);
            }
        });
        jMenuBar1.add(jMenu2);

        jMenu4.setText("Salir");
        jMenu4.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
            public void menuDeselected(javax.swing.event.MenuEvent evt) {
            }
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                jMenu4MenuSelected(evt);
            }
        });
        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(jLabel19))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(68, 68, 68)
                                .addComponent(comprar)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(54, 54, 54)
                                .addComponent(nombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel9)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel2)
                                                .addGap(14, 14, 14)))
                                        .addGap(36, 36, 36)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(total, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(carnesPrecio)
                                                .addGap(37, 37, 37)
                                                .addComponent(carneCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addGap(52, 52, 52)
                                        .addComponent(pastasPrecio)
                                        .addGap(37, 37, 37)
                                        .addComponent(pastasCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addGap(60, 60, 60)
                                        .addComponent(avesPrecio)
                                        .addGap(37, 37, 37)
                                        .addComponent(avesCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel18)
                                            .addComponent(jLabel7)
                                            .addComponent(jLabel6))
                                        .addGap(33, 33, 33)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(limpiezaPrecio)
                                            .addComponent(lacteosPrecio)
                                            .addComponent(licoresPrecio))
                                        .addGap(29, 29, 29)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(lacteosCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(licoresCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(limpiezaCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addGap(53, 53, 53)
                                        .addComponent(frutasPrecio)
                                        .addGap(37, 37, 37)
                                        .addComponent(frutasCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 23, Short.MAX_VALUE)))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(11, 11, 11))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(panaderiaPrecio)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(47, 47, 47)
                                .addComponent(panaderiaCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(nombre))
                .addGap(18, 18, 18)
                .addComponent(jLabel19)
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(carnesPrecio)
                        .addComponent(carneCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(frutasPrecio)
                    .addComponent(frutasCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(pastasPrecio)
                    .addComponent(pastasCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(avesCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(avesPrecio))))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(licoresCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(licoresPrecio)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel18)
                                .addGap(21, 21, 21))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lacteosPrecio)
                                    .addComponent(lacteosCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)))
                        .addComponent(jLabel6)))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(limpiezaCont, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(limpiezaPrecio)))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(panaderiaPrecio)
                    .addComponent(panaderiaCont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(total)
                    .addComponent(jLabel9))
                .addGap(35, 35, 35)
                .addComponent(comprar)
                .addGap(22, 22, 22))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenu5MenuSelected(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_jMenu5MenuSelected
        this.dispose();
        new Home(p,canasta).setVisible(true);
    }//GEN-LAST:event_jMenu5MenuSelected

    private void jMenu3MenuSelected(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_jMenu3MenuSelected
        this.dispose();
        new Saldo(p,canasta).setVisible(true);
    }//GEN-LAST:event_jMenu3MenuSelected

    private void carneContStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_carneContStateChanged
        float precio = 0;
        if(canasta.getCarnes()>0){
            precio = precio_descuento(canasta.getCarnes()-1, (float) p.getCarnes().getPrecioKG());
            System.out.println(precio);
            canasta.setPrecio_canasta(canasta.getPrecio_canasta()-precio_descuento(1, (float) p.getCarnes().getPrecioKG()));
            total.setText("$" + canasta.getPrecio_canasta());
            carnesPrecio.setText("$"+precio);
            carneCont.setModel(new SpinnerNumberModel(canasta.getCarnes()-1, 0, canasta.getCarnes()-1, 1));
            canasta.setCarnes(canasta.getCarnes()-1);
            JOptionPane.showMessageDialog(this, "Producto eliminado con éxito", "Proceso completado", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_carneContStateChanged

    private void frutasContStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_frutasContStateChanged
        float precio = 0;
        if(canasta.getFrutas()>0){
            precio = precio_descuento(canasta.getFrutas()-1, (float) p.getFrutas().getPrecioKG());
            canasta.setPrecio_canasta(canasta.getPrecio_canasta()-precio_descuento(1, (float) p.getFrutas().getPrecioKG()));
            total.setText("$" + canasta.getPrecio_canasta());
            frutasPrecio.setText("$"+precio);
            frutasCont.setModel(new SpinnerNumberModel(canasta.getFrutas()-1, 0, canasta.getFrutas()-1, 1));
            canasta.setFrutas(canasta.getFrutas()-1);
            JOptionPane.showMessageDialog(this, "Producto eliminado con éxito", "Proceso completado", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_frutasContStateChanged

    private void pastasContStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_pastasContStateChanged
        float precio = 0;
        if(canasta.getPastas()>0){
            precio = precio_descuento(canasta.getPastas()-1, (float) p.getPastas().getPrecioKG());
            canasta.setPrecio_canasta(canasta.getPrecio_canasta()-precio_descuento(1, (float) p.getPastas().getPrecioKG()));
            total.setText("$" + canasta.getPrecio_canasta());
            pastasPrecio.setText("$"+precio);
            pastasCont.setModel(new SpinnerNumberModel(canasta.getPastas()-1, 0, canasta.getPastas()-1, 1));
            canasta.setPastas(canasta.getPastas()-1);
            JOptionPane.showMessageDialog(this, "Producto eliminado con éxito", "Proceso completado", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_pastasContStateChanged

    private void avesContStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_avesContStateChanged
        float precio = 0;
        if(canasta.getAves()>0){
            precio = precio_descuento(canasta.getAves()-1, (float) p.getAves().getPrecioKG());
            canasta.setPrecio_canasta(canasta.getPrecio_canasta()-precio_descuento(1, (float) p.getAves().getPrecioKG()));
            total.setText("$" + canasta.getPrecio_canasta());
            avesPrecio.setText("$"+precio);
            avesCont.setModel(new SpinnerNumberModel(canasta.getAves()-1, 0, canasta.getAves()-1, 1));
            canasta.setAves(canasta.getAves()-1);
            JOptionPane.showMessageDialog(this, "Producto eliminado con éxito", "Proceso completado", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_avesContStateChanged

    private void lacteosContStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_lacteosContStateChanged
        float precio = 0;
        if(canasta.getLacteos()>0){
            precio = precio_descuento(canasta.getLacteos()-1, (float) p.getLacteos().getPrecioKG());
            canasta.setPrecio_canasta(canasta.getPrecio_canasta()-precio_descuento(1, (float) p.getLacteos().getPrecioKG()));
            total.setText("$" + canasta.getPrecio_canasta());
            lacteosPrecio.setText("$"+precio);
            lacteosCont.setModel(new SpinnerNumberModel(canasta.getLacteos()-1, 0, canasta.getLacteos()-1, 1));
            canasta.setLacteos(canasta.getLacteos()-1);
            JOptionPane.showMessageDialog(this, "Producto eliminado con éxito", "Proceso completado", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_lacteosContStateChanged

    private void licoresContStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_licoresContStateChanged
        float precio = 0;
        if(canasta.getLicores()>0){
            precio = precio_descuento(canasta.getLicores()-1, (float) p.getLicores().getPrecioKG());
            canasta.setPrecio_canasta(canasta.getPrecio_canasta()-precio_descuento(1, (float) p.getLicores().getPrecioKG()));
            total.setText("$" + canasta.getPrecio_canasta());
            licoresPrecio.setText("$"+precio);
            licoresCont.setModel(new SpinnerNumberModel(canasta.getLicores()-1, 0, canasta.getLicores()-1, 1));
            canasta.setLicores(canasta.getLicores()-1);
            JOptionPane.showMessageDialog(this, "Producto eliminado con éxito", "Proceso completado", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_licoresContStateChanged

    private void limpiezaContStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_limpiezaContStateChanged
        float precio = 0;
        if(canasta.getLimpieza()>0){
            precio = precio_descuento(canasta.getLimpieza()-1, (float) p.getLimpieza().getPrecioKG());
            canasta.setPrecio_canasta(canasta.getPrecio_canasta()-precio_descuento(1, (float) p.getLimpieza().getPrecioKG()));
            total.setText("$" + canasta.getPrecio_canasta());
            limpiezaPrecio.setText("$"+precio);
            limpiezaCont.setModel(new SpinnerNumberModel(canasta.getLimpieza()-1, 0, canasta.getLimpieza()-1, 1));
            canasta.setLimpieza(canasta.getLimpieza()-1);
            JOptionPane.showMessageDialog(this, "Producto eliminado con éxito", "Proceso completado", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_limpiezaContStateChanged

    private void panaderiaContStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_panaderiaContStateChanged
        float precio = 0;
        if(canasta.getPanaderia()>0){
            precio = precio_descuento(canasta.getPanaderia()-1, (float) p.getPanaderia().getPrecioKG());
            canasta.setPrecio_canasta(canasta.getPrecio_canasta()-precio_descuento(1, (float) p.getPanaderia().getPrecioKG()));
            total.setText("$" + canasta.getPrecio_canasta());
            panaderiaPrecio.setText("$"+precio);
            panaderiaCont.setModel(new SpinnerNumberModel(canasta.getPanaderia()-1, 0, canasta.getPanaderia()-1, 1));
            canasta.setPanaderia(canasta.getPanaderia()-1);
            JOptionPane.showMessageDialog(this, "Producto eliminado con éxito", "Proceso completado", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_panaderiaContStateChanged

    private void comprarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprarActionPerformed
        int stock = canasta.getAves()+canasta.getCarnes()+canasta.getFrutas()+canasta.getLacteos()+canasta.getLicores()+canasta.getLimpieza()+canasta.getPanaderia()+canasta.getPastas();
        if (stock > 0) {
            float compra = comprar(canasta.getPrecio_canasta());
            if (compra > 0) {
                Usuario user = p.getUsuario();
                user.setSaldo_final_en_pesos(user.getSaldo_final_en_pesos() - compra);
                
                articulos_comprados = canasta.getCarnesComprados();
                canasta.setPrecio_total(canasta.getPrecio_total() + compra);
                
                reducirStock();
                canasta.setUsuario(user);
                p.setUsuario(user);
                
                canasta.limpiar();
                resetLabel();
                resetSpinner();
                JOptionPane.showMessageDialog(this, "Compra realizado con éxito", "Proceso completado", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Fondos insuficiente", "No se pudo procesar su pago", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Las compras tienen que ser mayor a 1 unidad", "No se pudo procesar su pago", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_comprarActionPerformed

    private void jMenu2MenuSelected(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_jMenu2MenuSelected
        this.dispose();
        new Compras(p,canasta).setVisible(true);
    }//GEN-LAST:event_jMenu2MenuSelected

    private void jMenu4MenuSelected(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_jMenu4MenuSelected
        this.dispose();
        new Salir(p,canasta).setVisible(true);
    }//GEN-LAST:event_jMenu4MenuSelected

    private float comprar(float precio){
        return p.getUsuario().getSaldo_final_en_pesos() >= precio ? p.getUsuario().getSaldo_final_en_pesos()-precio : 0;
    }
    
    private void reducirStock(){
        Articulos articulo = null;
        if(Integer.parseInt(carneCont.getValue().toString())>0){
            articulo = new Articulos();
            articulo.setPrecioKG(Double.parseDouble(carnesPrecio.getText().substring(1)));
            articulo.setkilos(Integer.parseInt(carneCont.getValue().toString()));
            articulos_comprados.add(articulo);
            canasta.setCarnesComprados(articulos_comprados);
            p.getCarnes().setkilos(p.getCarnes().getkilos()-Integer.parseInt(carneCont.getValue().toString()));
        }
        
        if(Integer.parseInt(avesCont.getValue().toString())>0){
            articulo = new Articulos();
            articulo.setPrecioKG(Double.parseDouble(avesPrecio.getText().substring(1)));
            articulo.setkilos(Integer.parseInt(avesCont.getValue().toString()));
            articulos_comprados.add(articulo);
            canasta.setAvesComprados(articulos_comprados);
            p.getAves().setkilos(p.getAves().getkilos()-Integer.parseInt(avesCont.getValue().toString()));
        }
        
        if(Integer.parseInt(pastasCont.getValue().toString())>0){
            articulo = new Articulos();
            articulo.setPrecioKG(Double.parseDouble(pastasPrecio.getText().substring(1)));
            articulo.setkilos(Integer.parseInt(pastasCont.getValue().toString()));
            articulos_comprados.add(articulo);
            canasta.setPastasComprados(articulos_comprados);
            p.getPastas().setkilos(p.getPastas().getkilos()-Integer.parseInt(pastasCont.getValue().toString()));
        }
        
        if(Integer.parseInt(lacteosCont.getValue().toString())>0){
            articulo = new Articulos();
            articulo.setPrecioKG(Double.parseDouble(lacteosPrecio.getText().substring(1)));
            articulo.setkilos(Integer.parseInt(lacteosCont.getValue().toString()));
            articulos_comprados.add(articulo);
            canasta.setLacteosComprados(articulos_comprados);
            p.getLacteos().setkilos(p.getLacteos().getkilos()-Integer.parseInt(lacteosCont.getValue().toString()));
        }
        
         if(Integer.parseInt(limpiezaCont.getValue().toString())>0){
            articulo = new Articulos();
            articulo.setPrecioKG(Double.parseDouble(limpiezaPrecio.getText().substring(1)));
            articulo.setkilos(Integer.parseInt(limpiezaCont.getValue().toString()));
            articulos_comprados.add(articulo);
            canasta.setLimpiezaComprados(articulos_comprados);
            p.getLimpieza().setkilos(p.getLimpieza().getkilos()-Integer.parseInt(limpiezaCont.getValue().toString()));
        }
        
        if(Integer.parseInt(licoresCont.getValue().toString())>0){
            articulo = new Articulos();
            articulo.setPrecioKG(Double.parseDouble(licoresPrecio.getText().substring(1)));
            articulo.setkilos(Integer.parseInt(licoresCont.getValue().toString()));
            articulos_comprados.add(articulo);
            canasta.setLicoresComprados(articulos_comprados);
            p.getLicores().setkilos(p.getLicores().getkilos()-Integer.parseInt(licoresCont.getValue().toString()));
        }
        
        if(Integer.parseInt(panaderiaCont.getValue().toString())>0){
            articulo = new Articulos();
            articulo.setPrecioKG(Double.parseDouble(panaderiaPrecio.getText().substring(1)));
            articulo.setkilos(Integer.parseInt(panaderiaCont.getValue().toString()));
            articulos_comprados.add(articulo);
            canasta.setPanaderiaComprados(articulos_comprados);
            p.getPanaderia().setkilos(p.getPanaderia().getkilos()-Integer.parseInt(panaderiaCont.getValue().toString()));
        }
        
        if(Integer.parseInt(frutasCont.getValue().toString())>0){
            articulo = new Articulos();
            articulo.setPrecioKG(Double.parseDouble(frutasPrecio.getText().substring(1)));
            articulo.setkilos(Integer.parseInt(frutasCont.getValue().toString()));
            articulos_comprados.add(articulo);
            canasta.setFrutasComprados(articulos_comprados);
            p.getFrutas().setkilos(p.getFrutas().getkilos()-Integer.parseInt(frutasCont.getValue().toString()));
        }
        
    }
    
    
    private float precio_descuento(int num, float precio){
        return (float) (num*((precio)-(precio*descuento)));
    }
    
    private void resetLabel(){
         carnesPrecio.setText("");
        frutasPrecio.setText("");
        pastasPrecio.setText("");
        licoresPrecio.setText("");
        limpiezaPrecio.setText("");
        panaderiaPrecio.setText("");
        avesPrecio.setText("");
        lacteosPrecio.setText("");
        total.setText("");
    }
    
    private void cargarSpinner(){
        frutasCont.setModel(new SpinnerNumberModel(canasta.getFrutas(), 0,canasta.getFrutas(), 1));
        carneCont.setModel(new SpinnerNumberModel(canasta.getCarnes(), 0, canasta.getCarnes(), 1));
        panaderiaCont.setModel(new SpinnerNumberModel(canasta.getPanaderia(), 0, canasta.getPanaderia(), 1));
        licoresCont.setModel(new SpinnerNumberModel(canasta.getLicores(), 0, canasta.getLicores(), 1));
        limpiezaCont.setModel(new SpinnerNumberModel(canasta.getLimpieza(), 0, canasta.getLimpieza(), 1));
        lacteosCont.setModel(new SpinnerNumberModel(canasta.getLacteos(), 0, canasta.getLacteos(), 1));
        pastasCont.setModel(new SpinnerNumberModel(canasta.getPastas(), 0, canasta.getPastas(), 1));
        avesCont.setModel(new SpinnerNumberModel(canasta.getAves(), 0, canasta.getAves(), 1));
    }
    
    private void resetSpinner(){
        frutasCont.setModel(new SpinnerNumberModel(0, 0, 0, 1));
        carneCont.setModel(new SpinnerNumberModel(0, 0, 0, 1));
        panaderiaCont.setModel(new SpinnerNumberModel(0, 0, 0, 1));
        licoresCont.setModel(new SpinnerNumberModel(0, 0, 0, 1));
        limpiezaCont.setModel(new SpinnerNumberModel(0, 0, 0, 1));
        lacteosCont.setModel(new SpinnerNumberModel(0, 0, 0, 1));
        pastasCont.setModel(new SpinnerNumberModel(0, 0, 0, 1));
        avesCont.setModel(new SpinnerNumberModel(0, 0, 0, 1));
    }
 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSpinner avesCont;
    private javax.swing.JLabel avesPrecio;
    private javax.swing.JSpinner carneCont;
    private javax.swing.JLabel carnesPrecio;
    private javax.swing.JButton comprar;
    private javax.swing.JSpinner frutasCont;
    private javax.swing.JLabel frutasPrecio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JSpinner lacteosCont;
    private javax.swing.JLabel lacteosPrecio;
    private javax.swing.JSpinner licoresCont;
    private javax.swing.JLabel licoresPrecio;
    private javax.swing.JSpinner limpiezaCont;
    private javax.swing.JLabel limpiezaPrecio;
    private javax.swing.JLabel nombre;
    private javax.swing.JSpinner panaderiaCont;
    private javax.swing.JLabel panaderiaPrecio;
    private javax.swing.JSpinner pastasCont;
    private javax.swing.JLabel pastasPrecio;
    private javax.swing.JLabel total;
    // End of variables declaration//GEN-END:variables
}
